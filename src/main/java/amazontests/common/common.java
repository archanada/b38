package amazontests.common;

	import java.io.File;
	import java.io.FileInputStream;
	import java.io.IOException;
	import java.io.InputStream;
	import java.util.Properties;

	import org.apache.commons.io.FileUtils;
	import org.openqa.selenium.OutputType;
	import org.openqa.selenium.TakesScreenshot;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.chrome.ChromeDriver;

	public class common {
		public WebDriver driver = null;
		public Properties objects=null;
		public Properties environment=null;
		String path=System.getProperty("user.dir");

		public void initBrowser() {
			System.out.println("path: +path");
			// 1. Open the chrome browser
			System.setProperty("webdriver.chrome.driver", path+"\\src\\main\\resources\\drivers\\windows\\chrome\\chromedriver.exe");
			driver = new ChromeDriver();
			// 2. Navigate to https://www.rediff.com/
			driver.get(environment.getProperty("url"));
		}

		public void closeBrowsers() {
			// 8. close the browser
			driver.close();
		}
		//@BeforeSuite
		public void loadFiles() throws IOException{
			InputStream file=new FileInputStream(path+"/src/main/resources/ObjectLocators.properties");
			objects=new Properties();
			objects.load(file);
			
			InputStream f=new FileInputStream(path+"/src/main/resources/Environment.properties");
			environment=new Properties();
			environment.load(f);
		}
		//@AfterSuite
		public void clearFiles(){
			objects=null;
		}
		public void captureScreenshot() throws IOException{
			// Capture screenshot
			TakesScreenshot screen = (TakesScreenshot) driver;
			File sourceFile = screen.getScreenshotAs(OutputType.FILE);

			File destinationFile = new File(
					path+"/Screenshots/Screen1.png");

			FileUtils.copyFile(sourceFile, destinationFile);
		}
	}

