package com.amazontests.createaccount;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import amazontests.common.Excelreader;

public class create {

WebDriver driver=null;
	
	@BeforeMethod
	public void openbrowser() {
		
System.setProperty("webdriver.chrome.driver","C:\\Users\\pc\\eclipse-workspace\\SeleniumBasics\\chromedriver.exe");
		
	 driver = new ChromeDriver();
	
		driver.get("https://www.amazon.com");
	}
	
		@Test(priority=2,dependsOnMethods="verifyPageTitle",enabled=true)
		public void verifylogin() throws InterruptedException {
			Thread.sleep(1500);
		WebElement account=driver.findElement(By.xpath("//span[text()='Hello, Sign in']"));
		Actions actions=new Actions(driver);
		actions.moveToElement(account).build().perform();
		driver.findElement(By.linkText("Sign in")).click();
		Thread.sleep(1500);
		
	driver.findElement(By.name("email")).sendKeys("archanada123@gmail.com");
	driver.findElement(By.id("continue")).click();
		driver.findElement(By.name("password")).sendKeys("Archanada1999@#");
		driver.findElement(By.id("signInSubmit")).click();
		
		Thread.sleep(1500);
		String actualErrorMessage=driver.findElement(By.xpath("//div[@role='alert']")).getText();
		String expectedErrorMessage="Invalid Login & Password";
		System.out.println(actualErrorMessage);
		System.out.println(expectedErrorMessage);
		Assert.assertEquals(actualErrorMessage,expectedErrorMessage);
		}
		@DataProvider
		public Object[][] data() throws EncryptedDocumentException, IOException {
				Excelreader excel =new Excelreader();
			return excel.readExcel();
		}
			
			@AfterMethod
			public void closebrowser() {
				//8.close the browser
				driver.quit();

	}

}
